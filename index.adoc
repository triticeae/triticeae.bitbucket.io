:language: r
:source-highlighter: pygments
:pygments-linenums-mode: table
:toc2:
:numbered:
:experimental:
:data-uri:

++++
<link rel="stylesheet"  href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/3.1.0/css/font-awesome.min.css">
++++

:icons: font

The assembly pipeline has a new name. Please go to

https://tritexassembly.bitbucket.io[https://tritexassembly.bitbucket.io].
